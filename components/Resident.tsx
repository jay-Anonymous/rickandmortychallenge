import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { GET_RESIDENT } from '../graphql/queries';

interface IResident {
  id: string;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  image: string;
  origin: {
    name: string;
  };
  location: {
    name: string;
  };
}

interface IParams {
  id: string;
}

interface IResidentDetailsProps {
  resident: IResident;
}

const ResidentDetails: React.FC<IResidentDetailsProps> = ({ resident }) => {
  const [notes, setNotes] = React.useState('');

  const handleAddNote = () => {
    // Add the note to a database or local storage
  };

  return (
    <div>
      <h1>{resident.name}</h1>
      <p>{resident.status}</p>
      <img src={resident.image} alt={resident.name} />
      <p>{resident.species}</p>
      <p>{resident.gender}</p>
      <p>{resident.origin.name}</p>
      <p>{resident.location.name}</p>
      <textarea value={notes} onChange={(e) => setNotes(e.target.value)} />
      <button onClick={handleAddNote}>Add Note</button>
    </div>
  );
};

const ResidentDetailsContainer: React.FC = () => {
  const { id } = useParams();
  const { loading, error, data } = useQuery<{ resident: IResident }>(GET_RESIDENT, {
    variables: { id },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;

  return <ResidentDetails resident={data.resident} />;
};

export default ResidentDetailsContainer;