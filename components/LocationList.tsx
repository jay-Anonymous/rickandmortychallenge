import LocationList from "./LocationList";

const locations = [
  {
    id: 1,
    name: "Location 1",
    residents: [
      { id: 101, name: "Resident 1" },
      { id: 102, name: "Resident 2" }
    ]
  },
  {
    id: 2,
    name: "Location 2",
    residents: [
      { id: 201, name: "Resident 3" },
      { id: 202, name: "Resident 4" }
    ]
  }
];

function App() {
  return (
    <div>
      <h1>Locations</h1>
      <LocationList    locations={locations} />
    </div>
    );
}
export default App;

  