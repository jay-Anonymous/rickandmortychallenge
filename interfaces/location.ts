// interfaces/location.ts

export interface Location {
    pathname: string;
    id: string;
    name: string;
    type: string;
    dimension: string;
    residents: string[];
  }
