// interfaces/resident.ts

export interface Resident {
    id: string;
    name: string;
    status: string;
    species: string;
    type: string;
    gender: string;
    image: string;
    origin: {
      name: string;
    };
    location: {
      name: string;
    };
  }