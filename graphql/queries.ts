// graphql/queries.ts
import { gql } from '@apollo/client';

export const GET_RESIDENT = gql`
  query GetResident($id: ID!) {
    resident(id: $id) {
      id
      name
      status
      species
      type
      gender
      image
      origin {
        name
      }
      location {
        name
      }
    }
  }
`;