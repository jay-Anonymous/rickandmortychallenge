import LocationList from '../components/LocationList';

const Home = () => {
  return (
    <div className="container mx-auto">
      <h1 className="text-4xl font-bold mb-4">Rick & Morty Locations</h1>
      <LocationList />
    </div>
  );
};

export default Home;