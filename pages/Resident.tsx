import { gql } from '@apollo/client/core';

// Define the GET_RESIDENT query using the gql template literal tag
const GET_RESIDENT_QUERY = gql`
  query GET_RESIDENT($id: ID!) {
    resident(id: $id) {
      id
      name
      age
      address
    }
  }
`;

// Define the Resident interface using TypeScript
interface Resident {
  id: string;
  name: string;
  age: number;
  address: string;
}