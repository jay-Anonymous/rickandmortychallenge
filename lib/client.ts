import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
//Apollo client configuration

const client = new ApolloClient({
  link: createHttpLink({
    uri: 'https://rickandmortyapi.com/graphql',
  }),
  cache: new InMemoryCache(),
});

export default client;